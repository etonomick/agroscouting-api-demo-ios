//
//  ViewController.swift
//  Agroscouting
//
//  Created by Valery Stepanov on 26/05/2018.
//  Copyright © 2018 Valery Stepanov. All rights reserved.
//

import UIKit

class FieldsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var fields: [Field]!
    
    var fieldsTableView: UITableView!
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Поля"
        
        fields = []
        
        fieldsTableView = UITableView(frame: view.frame, style: .plain)
        fieldsTableView.register(FieldTableViewCell.self, forCellReuseIdentifier: "field_cell")
        fieldsTableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "headerFooterView")
        fieldsTableView.delegate = self
        fieldsTableView.dataSource = self
        fieldsTableView.isHidden = true
        view.addSubview(fieldsTableView)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.center = view.center
        activityIndicator.color = UIColor.lightGray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        
        Requests.shared.getFields { (fields) in
            for field in fields {
                self.fields.append(Field.init(field: field))
            }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.fieldsTableView.reloadData()
                self.fieldsTableView.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fieldCell = tableView.dequeueReusableCell(withIdentifier: "field_cell", for: indexPath)
        
        let field: Field = fields[indexPath.row]
        
        fieldCell.textLabel?.text = field.fieldName
        fieldCell.detailTextLabel?.text = "\(field.id)"
        
        return fieldCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(FieldDetailsViewController(field: fields[indexPath.row]), animated: true)
    }

}

