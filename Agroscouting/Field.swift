//
//  Field.swift
//  Agroscouting
//
//  Created by Valery Stepanov on 27/05/2018.
//  Copyright © 2018 Valery Stepanov. All rights reserved.
//

import Foundation

struct Field {
    let id: Int
    let fieldName: String   // Название поля
    let locNote: String     // Описание
    let fieldNumb: String   // Номер поля
    let stypeCode: String   // Тип почвы
    let deptName: String    // Подразделение
    let wArea: Double       // Площадь по геозоне
    let accArea: Double     // Учетная площадь
    
    var coordinates: [[Any]]!
    var rows: [String: Any]!
}

extension Field {
    init(field: [String: Any]) {
        
        self.rows = [:]
        self.coordinates = []
        
        self.id = field["id"] as? Int ?? 0
        self.fieldName = field["field_name"] as? String ?? ""
        
        self.locNote = field["loc_note"] as? String ?? ""
        self.rows.updateValue(self.locNote, forKey: "Описание")
        
        self.fieldNumb = field["field_numb"] as? String ?? ""
        self.rows.updateValue(self.fieldNumb, forKey: "Номер поля")
        
        self.stypeCode = field["stype_code"] as? String ?? ""
        self.rows.updateValue(self.stypeCode, forKey: "Тип почвы")
        
        self.deptName = field["dept_name"] as? String ?? ""
        self.rows.updateValue(deptName, forKey: "Подразделение")
        
        self.wArea = field["w_area"] as? Double ?? 0
        self.rows.updateValue("\(self.wArea)", forKey: "Площадь по геозоне")
        
        self.accArea = field["acc_area"] as? Double ?? 0
        self.rows.updateValue("\(self.accArea)", forKey: "Учетная площадь")
        
        self.coordinates = (((field["geozones"] as? [[String: Any]] ?? []).first?["geometry"] as? [String: Any] ?? [:])["coordinates"] as? [[Any]] ?? []).first as? [[Any]] ?? []
        
    }
}
