//
//  Requests.swift
//  Agroscouting
//
//  Created by Valery Stepanov on 27/05/2018.
//  Copyright © 2018 Valery Stepanov. All rights reserved.
//

import UIKit

class Requests: NSObject {
    
    static let shared = Requests.init()
    
    let baseApiUrl: URL = URL(string: "http://217.23.158.19:8080/ords/agrocore/mobile_test/field/")!
    var urlRequest: URLRequest!
    var urlSession: URLSession!
    
    override init() {
        urlRequest = URLRequest(url: baseApiUrl)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlSession = URLSession.shared
    }
    
    func getFields(completionHandler: @escaping (_ fields: [[String: Any]]) -> Void) {
        urlRequest.httpBody = "{\"method\":\"get_fields\"}".data(using: .utf8)
        urlSession.dataTask(with: urlRequest) { (data, response, error) in
            completionHandler(try! JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] ?? [])
        }.resume()
    }
    
    func getField(id: Int, completionHandler: @escaping (_ field: [String: Any]) -> Void) {
        urlRequest.httpBody = "{\"method\":\"get_field\",\"id\":\(id)}".data(using: .utf8)
        urlSession.dataTask(with: urlRequest) { (data, response, error) in
            // print(try! JSONSerialization.jsonObject(with: data!, options: []))
            completionHandler(try! JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] ?? [:])
        }.resume()
    }

}
