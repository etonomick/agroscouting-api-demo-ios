//
//  FieldDetailsViewController.swift
//  Agroscouting
//
//  Created by Valery Stepanov on 27/05/2018.
//  Copyright © 2018 Valery Stepanov. All rights reserved.
//

import UIKit
import GoogleMaps

class FieldDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var field: Field!
    
    var fieldTableView: UITableView!
    var activityIndicator: UIActivityIndicatorView!
    
    var camera: GMSCameraPosition!
    var mapView: GMSMapView!
    
    init(field: Field) {
        super.init(nibName: nil, bundle: nil)
        self.field = field
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = field.fieldName
        view.backgroundColor = UIColor.white
        
        fieldTableView = UITableView(frame: view.frame, style: .plain)
        fieldTableView.register(FieldTableViewCell.self, forCellReuseIdentifier: "cell")
        fieldTableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "headerFooterView")
        fieldTableView.delegate = self
        fieldTableView.dataSource = self
        
        mapView = GMSMapView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 250.0))
        fieldTableView.tableHeaderView = mapView
        
        fieldTableView.tableFooterView = UIView(frame: CGRect.zero)
        fieldTableView.isHidden = true
        view.addSubview(fieldTableView)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.center = view.center
        activityIndicator.color = UIColor.lightGray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        
        Requests.shared.getField(id: field.id) { (field) in
            self.field = Field.init(field: field)
            self.camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(exactly: self.field.coordinates.first![1] as? Double ?? 0)!, longitude: CLLocationDegrees(exactly: self.field.coordinates.first![0] as? Double ?? 0)!, zoom: 12.0)
            self.mapView.camera = self.camera
            let fieldPath = GMSMutablePath.init()
            for coordinate in self.field.coordinates {
                fieldPath.addLatitude(CLLocationDegrees(exactly: coordinate[1] as? Double ?? 0)!, longitude: CLLocationDegrees(exactly: coordinate[0] as? Double ?? 0)!)
            }
            let fieldRectangle = GMSPolyline(path: fieldPath)
            fieldRectangle.map = self.mapView
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.fieldTableView.reloadData()
                self.fieldTableView.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return field.rows.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        
        let key: String = field.rows.keys.sorted()[indexPath.row]
        
        cell.textLabel?.text = field.rows[key] as? String ?? ""
        cell.detailTextLabel?.text = key
        
        return cell
    }

}
