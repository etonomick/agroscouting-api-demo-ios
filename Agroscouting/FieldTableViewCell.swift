//
//  FieldTableViewCell.swift
//  Agroscouting
//
//  Created by Valery Stepanov on 27/05/2018.
//  Copyright © 2018 Valery Stepanov. All rights reserved.
//

import UIKit

class FieldTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

}
